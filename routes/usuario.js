// Armamos las Rutas para  nuestro Usuario

const express = require('express');
const router = express.Router();
const usuarioController = require('../controllers/UsuarioController');
const { check } = require("express-validator");



// rutas CRUD

router.get('/', usuarioController.mostrarUsuarios);
router.post('/', usuarioController.crearUsuario);
router.get('/:id', usuarioController.obtenerUsuario);
router.put('/:id', usuarioController.actualizarUsuario);
router.delete('/:id', usuarioController.eliminarUsuario);

module.exports = router;

