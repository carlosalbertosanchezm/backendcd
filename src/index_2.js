const express = require("express");
const conectarDB = require("../config/db");
const cors = require("cors");

//crear el servidor
const app = express();

//conectar a la base de datos
conectarDB();

//habilitar cors
app.use(cors());

//Habilite express.json
app.use(express.json({ extended: true }));

const PORT = process.env.PORT || 4000;

//importar rutas
app.use('/api/productos', require('../routes/producto'));
app.use('/api/clientes', require('../routes/cliente'));
app.use('/api/usuario', require('../routes/usuarios'));
app.use('/api/auth', require('../routes/auth'));

// arrancar la app
app.listen(PORT, () => {
  console.log(`El servidor está funcionando en el puerto ${PORT} `);
});
